package com.mindgame.scanner.app02;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore.Images.Media;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

public class Scanning extends Activity implements OnClickListener,
        SurfaceHolder.Callback, Camera.PictureCallback, AnimationListener {
    SurfaceView cameraView;
    SurfaceHolder surfaceHolder;
    Camera camera;
    int normal;
    TextView again, ok;
    boolean AutoBrightnessOpen = false;
    private static final int CAMERA_REQUEST = 2;
    Handler timerUpdateHandler;
    boolean timelapseRunning = false;
    int currentTime = 0;
    final int SECONDS_BETWEEN_PHOTOS = 60;
    private Bitmap bitmap;
    //private ImageView imageView;
    RelativeLayout rl, rl1, rl12;
    private TextView ba, ba1;
    Bitmap img, b1;
    public static Bitmap b;
    ImageView scanner;
    Animation moveleft;
    String fullPath;
    ImageView img1;
    public static File file;
    ByteArrayOutputStream bs;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    SharedPreferences pref;
    int genderValue, organValue, pregnency;

    AdClass ad = new AdClass();
    int currentapiVersion = android.os.Build.VERSION.SDK_INT;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scaning);
        int permissionCheck = ContextCompat.checkSelfPermission(Scanning.this,
                Manifest.permission.WRITE_CALENDAR);
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            check();
        }

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        genderValue = pref.getInt("genderval", 0);
        organValue = pref.getInt("organ", 0);
        pregnency = pref.getInt("pregency", 0);

        rl = (RelativeLayout) findViewById(R.id.relative11);
        rl1 = (RelativeLayout) findViewById(R.id.relativeLayout01);
        rl12 = (RelativeLayout) findViewById(R.id.relative12);

        ba = (TextView) findViewById(R.id.camera1);
        ba1 = (TextView) findViewById(R.id.ok);
        again = (TextView) findViewById(R.id.again);
        //imageView = (ImageView) findViewById(R.id.image_next_main_activity);
        scanner = (ImageView) findViewById(R.id.scanner);
        ok = (TextView) findViewById(R.id.ok2);
        scanner.setImageResource(R.mipmap.scan_ani);
        moveleft = AnimationUtils.loadAnimation(Scanning.this,
                R.anim.righttoleft);

        moveleft.setAnimationListener(this);
        // ba.setVisibility(View.INVISIBLE);
        ba1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                rl.setVisibility(View.INVISIBLE);

            }
        });

        ba.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // take the picture
                camera.takePicture(null, null, null, Scanning.this);
                rl1.setVisibility(View.INVISIBLE);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {


                        rl12.setDrawingCacheEnabled(true);

                        try {


                            rl12.buildDrawingCache(true);
                            b = Bitmap.createBitmap(rl12
                                    .getDrawingCache());
                            rl12.setDrawingCacheEnabled(false);
                            ImageView img = (ImageView) findViewById(R.id.img1);
                            img.setImageBitmap(b);


                        } catch (Exception e) {
// TODO: handle exception
                        } finally {
//ad.AdMobInterstitial2(Scanning.this);
                            ok.setVisibility(View.VISIBLE);
                            again.setVisibility(View.VISIBLE);

// startAppAd.loadAd();
// startAppAd.showAd();
                        }
                    }


                }, 1000);

            }
        });
        again.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                rl12.setVisibility(View.INVISIBLE);
                rl1.setVisibility(View.VISIBLE);
            }
        });

        ok.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                rl12.setVisibility(View.VISIBLE);
                scanner.setVisibility(View.VISIBLE);

                scanner.startAnimation(moveleft);
                ok.setVisibility(View.INVISIBLE);
                again.setVisibility(View.INVISIBLE);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // ok.setVisibility(View.INVISIBLE);
                        // again.setVisibility(View.INVISIBLE);
                        scanner.setImageResource(R.drawable.scan);

                        AnimationDrawable startAnimation = (AnimationDrawable) scanner
                                .getDrawable();
                        startAnimation.start();

                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                // TODO Auto-generated method stub
                                if (genderValue == 0) {

                                    organValue=new Random().nextInt(3);




                                    if (organValue == 0) {
                                        scanner.setImageResource(R.mipmap.kidney);
                                    } else if (organValue == 1) {
                                        scanner.setImageResource(R.mipmap.stomach);
                                    } else if (organValue == 2) {
                                        scanner.setImageResource(R.mipmap.spleen);
                                    } else if (organValue == 3) {
                                        scanner.setImageResource(R.mipmap.thyroid);
                                    }
                                } else {
                                    organValue=new Random().nextInt(4);

                                    if (organValue == 0) {
                                        scanner.setImageResource(R.mipmap.kidney);
                                    } else if (organValue==1) {
                                        scanner.setImageResource(R.mipmap.pregnancy);
                                    } else if (organValue ==2) {
                                        scanner.setImageResource(R.mipmap.stomach);
                                    } else if (organValue == 3) {
                                        scanner.setImageResource(R.mipmap.spleen);
                                    } else if (organValue == 4) {
                                        scanner.setImageResource(R.mipmap.thyroid);
                                    }
                                }
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {

                                        rl12.setDrawingCacheEnabled(true);

                                        try {

                                            ok.setVisibility(View.INVISIBLE);
                                            again.setVisibility(View.INVISIBLE);
                                            rl12.buildDrawingCache(true);
                                            b = Bitmap.createBitmap(rl12
                                                    .getDrawingCache());
                                            rl12.setDrawingCacheEnabled(false);
                                            ImageView img = new ImageView(
                                                    Scanning.this);
                                            img.setImageBitmap(b);
                                            BitmapDrawable drawable = (BitmapDrawable) img
                                                    .getDrawable();
                                            Bitmap b1 = drawable.getBitmap();

                                            bs = new ByteArrayOutputStream();
                                            b1.compress(
                                                    Bitmap.CompressFormat.PNG,
                                                    100, bs);

                                            saveImageToExternalStorage(b);

                                        } catch (Exception e) {
                                            // TODO: handle exception
                                        } finally {
                                            ad.AdMobInterstitial(Scanning.this);
                                            //  ad.RevMobFullPageAd(Scanning.this);
                                           Intent i = new Intent(
                                                    getApplicationContext(), ShareImage.class);

                                            //i.putExtra("byteArray", bs.toByteArray());

                                            startActivity(i);
                                            //startAppAd.loadAd();
                                            // startAppAd.showAd();
                                        }
                                    }

                                    private void saveImageToExternalStorage(
                                            Bitmap b2) {
                                        // TODO Auto-generated method stub

                                        // TODO Auto-generated method stub

                                        fullPath = Environment
                                                .getExternalStorageDirectory()
                                                .getAbsolutePath();
                                        try {
                                            File dir = new File(fullPath);
                                            if (!dir.exists()) {
                                                dir.mkdirs();
                                            }
                                            OutputStream fOut = null;
                                            file = new File(fullPath,
                                                    "image.jpeg");

                                            fOut = new FileOutputStream(file);
                                            // 100 means no compression, the
                                            // lower you
                                            // go, the stronger
                                            // the compression

                                            b2.compress(
                                                    Bitmap.CompressFormat.JPEG,
                                                    100, fOut);
                                            fOut.flush();
                                            fOut.close();

                                        } catch (Exception e) {
                                            Log.e("saveToExternalStorage()",
                                                    e.getMessage());
                                        }

                                    }

                                }, 1000);
                            }
                        }, 3000);

                    }

                }, 2000);

            }
        });

        cameraView = (SurfaceView) this.findViewById(R.id.CameraView);
        surfaceHolder = cameraView.getHolder();
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        surfaceHolder.addCallback(this);

        timerUpdateHandler = new Handler();

    }

    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
                Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }

    private Runnable timerUpdateTask = new Runnable() {
        public void run() {
            if (currentTime < SECONDS_BETWEEN_PHOTOS) {
                currentTime++;
            } else {

                currentTime = 0;
            }

            timerUpdateHandler.postDelayed(timerUpdateTask, 1000);

        }
    };

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        camera.startPreview();
    }

    public boolean isAutoBrightness(ContentResolver aContentResolver) {
        boolean automicBrightness = false;
        try {
            automicBrightness = Settings.System.getInt(aContentResolver,
                    Settings.System.SCREEN_BRIGHTNESS_MODE) == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
        }
        return automicBrightness;
    }


    public void surfaceCreated(SurfaceHolder holder) {

        camera = Camera.open();
        try {
            camera.setPreviewDisplay(holder);
            Camera.Parameters parameters = camera.getParameters();

            if (this.getResources().getConfiguration().orientation != Configuration.ORIENTATION_LANDSCAPE) {
                parameters.set("orientation", "portrait");
                camera.setDisplayOrientation(90);
            }
            camera.setParameters(parameters);
        } catch (IOException exception) {
            camera.release();
        }

    }

    @Override
    public void onBackPressed() {

      /*  Intent i = new Intent(getApplicationContext(), SelectGender.class);
        Bundle b = new Bundle();
        startActivity(i);
*/
        super.onBackPressed();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        camera.stopPreview();
        camera.release();
    }

    public void onPictureTaken(byte[] data, Camera camera) {
        Uri imageFileUri = getContentResolver().insert(
                Media.EXTERNAL_CONTENT_URI, new ContentValues());

        try {
            FileOutputStream outStream = new FileOutputStream(String.format(
                    "/sdcard/image.jpg", System.currentTimeMillis()));
            outStream.write(data);
            outStream.close();
            Log.d("Log", "onPictureTaken - wrote bytes: " + data.length);
            File sdcard = Environment.getExternalStorageDirectory();
            b1 = BitmapFactory.decodeFile("/sdcard/image.jpg");
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            img = Bitmap.createBitmap(b1, 0, 0, b1.getWidth(), b1.getHeight(),
                    matrix, true);

            RotateBitmap(b1, 90);
            Drawable d = new BitmapDrawable(getResources(), img);
            rl12.setVisibility(View.VISIBLE);
            rl12.setBackgroundDrawable(d);

            // Toast t = Toast.makeText(this, "Saved JPEG!",
            // Toast.LENGTH_SHORT);
            // t.show();
        } catch (Exception e) {
            Toast t = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            t.show();
        }
        camera.startPreview();
    }

    public Bitmap RotateBitmap(Bitmap source, float angle) {

        return img;
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    }

    public void check() {
        if (currentapiVersion >= Build.VERSION_CODES.M) {
            // Do something for lollipop and above versions
            if (ContextCompat.checkSelfPermission(Scanning.this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(Scanning.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(Scanning.this,
                        Manifest.permission.CAMERA) && ActivityCompat.shouldShowRequestPermissionRationale(Scanning.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    // Show an expanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(Scanning.this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }
        } else {
            // do something for phones running an SDK before lollipop
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}
