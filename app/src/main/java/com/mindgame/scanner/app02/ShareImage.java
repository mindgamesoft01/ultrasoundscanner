package com.mindgame.scanner.app02;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ShareImage extends Activity {

    String path, fullPath;
    ImageView img;
    Bitmap b1;
    File file;
    int flag = 1;
    int flag_save = 0;
    int isCheckedValue = 1;
    int d = 0;

    AdClass ad = new AdClass();

    LinearLayout strip1, layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);


        setContentView(R.layout.shareimage);

        layout = (LinearLayout) findViewById(R.id.admobAD);
        strip1 = ad.layout_strip(ShareImage.this);
        layout.addView(strip1);
        ad.AdMobBanner1(ShareImage.this);

        img = (ImageView) findViewById(R.id.image);

        //File sdcard = Environment.getExternalStorageDirectory();

        //path = butelkaActivity.file.getAbsolutePath();
        //b1 = BitmapFactory.decodeFile(path);

        img.setImageBitmap(Scanning.b);

        findViewById(R.id.textshare).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                share();
            }
        });

        findViewById(R.id.textsave).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //startAppAd.showAd();
                //startAppAd.loadAd();
                if (flag_save == 0) {

                    save();
                    flag_save = 1;
                    d = 1;
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Image is Already Saved", Toast.LENGTH_LONG).show();
                }
            }
        });
        findViewById(R.id.texthome).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent i = new Intent(ShareImage.this, Start_Aom.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);


            }
        });
    }

    void share() {

        // save();
        if (flag_save == 1) {

            img.setDrawingCacheEnabled(true);
            img.buildDrawingCache(true);
            Bitmap icon = img.getDrawingCache();
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            icon.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            File f = new File(Environment.getExternalStorageDirectory()
                    + File.separator + "temporary_file.jpg");
            if (f.exists())
                f.delete();
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());

            } catch (IOException e) {
                e.printStackTrace();
            }

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/png");

            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            // Uri uri = Uri.parse("file:///sdcard/temporary_file.jpg");
            shareIntent.putExtra(Intent.EXTRA_STREAM,
                    Uri.parse("file:///sdcard/temporary_file.jpg"));
            shareIntent.putExtra(Intent.EXTRA_TITLE, "Ultrasound  Scanner");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Ultrasound  Scanner");
            shareIntent
                    .putExtra(
                            Intent.EXTRA_TEXT, ""

                                    + "https://play.google.com/store/apps/details?id=" + getApplication().getPackageName());

            try {
                startActivity(Intent.createChooser(shareIntent,
                        "Send your image"));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(),
                        " Whatsapp have not been installed.", Toast.LENGTH_LONG)
                        .show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Save Image First",
                    Toast.LENGTH_LONG).show();
        }
    }

    void save() {

        try {
            img.setDrawingCacheEnabled(true);
            img.buildDrawingCache(true);
            Bitmap common = Bitmap.createBitmap(img.getDrawingCache());

            img.setDrawingCacheEnabled(false);

            saveImageToExternalStorage(common);
            Toast.makeText(getApplicationContext(),
                    "Image Saved in Ultra Scan Folder", Toast.LENGTH_LONG).show();

            isCheckedValue = 2;
        } catch (Exception e) {
            // TODO: handle exception
            Toast.makeText(getApplicationContext(),
                    "Some Thing Went Worng Please Restart The Application",
                    Toast.LENGTH_SHORT).show();
        }

    }

    void saveImageToExternalStorage(Bitmap b) {

        // add_camera_gallery_two.setVisibility(View.INVISIBLE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String timeStamp = sf.format(new Date());

        String fullPath = Environment.getExternalStorageDirectory()
                .getAbsolutePath() + "/Ultra Sound Scanner/";
        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            OutputStream fOut = null;
            File file = new File(fullPath, "picture" + timeStamp + ".jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

    }


    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        {


            startActivity(new Intent(getApplicationContext(), Start_Aom.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));

        }
    }
}
