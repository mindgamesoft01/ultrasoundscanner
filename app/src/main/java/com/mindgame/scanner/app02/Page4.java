package com.mindgame.scanner.app02;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;


/**
 * Created by NAG on 23-Jan-17.
 */

public class Page4 extends Activity {
    int val1;
    public static int gender;
    TextView male,female;
    AdClass ads = new AdClass();
    Button netxt_gender;
    LinearLayout layout, strip;

    singleton_admob sc= singleton_admob.getInstance();
    Button btn1;
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page ="ca-app-pub-4951445087103663/6407075341";

    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 ="ca-app-pub-4951445087103663/6407075341";

    Boolean connected;
    CustomDialogClass cd;
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();
    TextView terms_privacy;
    SharedPreferences pref;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.page4);
        netxt_gender = (Button) findViewById(R.id.next_gender);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);


        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ads.layout_strip(this);
        layout.addView(strip);
        ads.AdMobBanner1(Page4.this);

        cd = new CustomDialogClass(Page4.this, 1);
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);

        final AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        String app_status = sc.app_mode();

      //  Toast.makeText(getApplicationContext(), "App_Sttus :" + app_status, Toast.LENGTH_SHORT).show();
        Log.d("APP_STATAUS :", app_status);


        male = (TextView) findViewById(R.id.male);
        female = (TextView) findViewById(R.id.female);
        male.setBackgroundResource(R.drawable.maleunselected);
        female.setBackgroundResource(R.drawable.femaleunselected);

        val1 = 0;

        findViewById(R.id.male).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val1 = 1;
                gender = 1;

                male.setBackgroundResource(R.drawable.male_selected);
                female.setBackgroundResource(R.drawable.femaleunselected);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("genderval", 1);
                editor.commit();

            }
        });

        findViewById(R.id.female).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                val1 = 1;
                gender = 0;
                male.setBackgroundResource(R.drawable.maleunselected);
                female.setBackgroundResource(R.drawable.female_selected);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("genderval", 0);
                editor.commit();
            }
        });

        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

            netxt_gender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //interstitialAd.setAdUnitId(interstitial0);

                    Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                        Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                        //cd.show();
                    }
                    if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                        Boolean ad1_status = singleton_admob.getInstance().get_ad_status(1);
                        pd.show();
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            if (val1 == 1) {
                                startActivity(new Intent(getApplicationContext(), Scanning.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                            } else {
                                Toast.makeText(getApplicationContext(), "Please  Selct Gender", Toast.LENGTH_LONG).show();
                            }
                            pd.dismiss();


                        }


                        if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    //Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                    if (val1 == 1) {
                                        startActivity(new Intent(getApplicationContext(), Scanning.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Please  Select Gender", Toast.LENGTH_LONG).show();
                                    }
                                    pd.dismiss();


                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);

                                    if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                        //cd.show();
                                        pd.dismiss();
                                    }
                                    if (val1 == 1) {
                                        startActivity(new Intent(getApplicationContext(), Scanning.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                                    } else {
                                        Toast.makeText(getApplicationContext(), "Please  Select Gender", Toast.LENGTH_LONG).show();
                                    }

                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_admob.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp
                                    singleton_admob.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });




            findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    onBackPressed();
                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), Page2.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
    }

}
