package com.mindgame.scanner.app02;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by kalyani on 8/8/2017.
 */

public class CustomDialogClass extends Dialog implements
        View.OnClickListener {

    public Activity c;
    public Dialog d;
    public TextView ok;
    TextView dis;
    Button rate,exit;
    int tag;

    public CustomDialogClass(Activity a, int i) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.tag=i;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        dis=(TextView)findViewById(R.id.txt_dis);
        ok = (TextView) findViewById(R.id.txt_ok);
        rate=(Button)findViewById(R.id.rate);
        exit=(Button)findViewById(R.id.exit);


        ok.setOnClickListener(this);
        rate.setOnClickListener(this);
        if(tag==0){
            ok.setVisibility(View.VISIBLE);
            dis.setText(R.string.page_refresh);

        }
        if(tag==1){
            ok.setVisibility(View.VISIBLE);
            dis.setText(R.string.network_error);

        }
        if(tag==2){
            rate.setVisibility(View.VISIBLE);
            rate.setVisibility(View.VISIBLE);
            dis.setText(R.string.rate_exit);

        }


        }






    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_ok:
                dismiss();
                break;
            case R.id.rate:
                Intent i = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=com.am.jwelery.collection.designs"));

                c.startActivity(i);
            case R.id.exit:
                Intent j = new Intent(Intent.ACTION_MAIN);
                j.addCategory(Intent.CATEGORY_HOME);
                j.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(j);

            default:
                break;
        }
        dismiss();
    }
}
