package com.mindgame.scanner.app02;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;


public class Start_Aom extends Activity {


    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();

    singleton_admob sc= singleton_admob.getInstance();
    Button btn1;
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page ="ca-app-pub-4951445087103663/6407075341";

    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 ="ca-app-pub-4951445087103663/6407075341";

    Boolean connected;
    CustomDialogClass cd;
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();
    TextView terms_privacy;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.page1);
        btn1 = (Button) findViewById(R.id.start);

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);

        layout1 = (LinearLayout) findViewById(R.id.admob1);
        strip1 = ad.layout_strip(this);
        layout1.addView(strip1);
        ad.AdMobBanner(this);

        cd=new CustomDialogClass(Start_Aom.this,1);
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);

        final AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        String app_status = sc.app_mode();

        //Toast.makeText(getApplicationContext(), "App_Sttus :" + app_status, Toast.LENGTH_SHORT).show();
        Log.d("APP_STATAUS :", app_status);
        if (app_status.equals("TEST")) {
            btn1.setVisibility(View.INVISIBLE);

        }
        if (app_status.equals("PROD")) {

            if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                btn1.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                            //cd.show();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_admob.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(Start_Aom.this, Page2.class);
                                Bundle b = new Bundle();
                                b.putInt("flag", 1);
                                i.putExtras(b);
                                singleton_admob.getInstance().lastIntestitial_loadded = new Date().getTime(); // Save the Ad loaded timestamp
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(Start_Aom.this, Page2.class);
                                        Bundle b = new Bundle();
                                        b.putInt("flag", 1);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();




                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE ){
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(Start_Aom.this, Page2.class);
                                        Bundle b = new Bundle();
                                        b.putInt("flag", 1);
                                        j.putExtras(b);
                                        startActivity(j);

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_admob.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_admob.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });





                findViewById(R.id.rate).setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        Intent i = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName()));

                        startActivity(i);

                    }
                });
            }
            terms_privacy=(TextView)findViewById(R.id.privacy);
            terms_privacy.setPaintFlags(terms_privacy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            terms_privacy.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Start_Aom.this, PrivacyPolicy.class);
                    startActivity(i);
                }
            });


        }
    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        // startAppAd.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        // startAppAd.onResume();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

			/*Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);*/
            cd = new CustomDialogClass(Start_Aom.this, 2);

        }

        return super.onKeyDown(keyCode, event);
    }


}


